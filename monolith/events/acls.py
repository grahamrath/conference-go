from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    url = 'https://api.pexels.com/v1/search'
    headers = {'Authorization': PEXELS_API_KEY}
    params = {"query": city + " " + state, "per_page": 70}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    return {"photo": content["photos"][67]["src"]["original"]}


def get_weather_data(city, state):
    url_weather = 'http://api.openweathermap.org/geo/1.0/direct'
    country_code = 840  # pandas + pgeo code would pull from the global country code DB
    params = {
        "q": [city, state, country_code],
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url_weather, params=params)
    content = json.loads(response.content)
    # print("\n\n\n\n", response.content, "\n\n\n\n")
    lon = content[0]["lon"]
    # print(lon)
    lat = content[0]["lat"]
    # print(lat)

    url_weather = 'https://api.openweathermap.org/data/2.5/weather'
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(url_weather, params=params)
    # print("\n\n\n\n", response.content, "\n\n\n\n")
    content = json.loads(response.content)
    if content:
        temp = content["main"]["temp"]
        description = content["weather"][0]["description"]
        weather = {
            "temp": temp,
            "description": description,
        }
    else:
        weather = None

    return weather
